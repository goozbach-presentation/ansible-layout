OUTPUTDIR ?= output
.PHONY: all clean server 
.SECONDARY:

# needs to run in the container
all: $(OUTPUTDIR)/index.html

# needs to run in the container
$(OUTPUTDIR)/index.html: slides.adoc Makefile $(OUTPUTDIR)/node_modules
	asciidoctor-revealjs -o $@ -a skip-front-matter -a revealjsdir=node_modules/reveal.js/ $<

$(OUTPUTDIR)/node_modules:
	mkdir -p $(OUTPUTDIR) 
	cp package* $(OUTPUTDIR)
	cd $(OUTPUTDIR) && npm i

# should run outside of the container
server:
	cd $(OUTPUTDIR)/ && python2 -m SimpleHTTPServer 8000

clean:
	rm -rf $(OUTPUTDIR)/*.html
